let body = document.querySelector('body'),
  	topbar = document.querySelector('#topbar')
    html = document.querySelector('html')

html.classList.remove('noJs')

window.addEventListener("scroll", function(){
  if(window.scrollY < 10){
    topbar.classList.remove('xs')
  } else{
    topbar.classList.add('xs')
  }
});

const form = document.getElementById('newsletter');
form.addEventListener("submit", function(e) {
  e.preventDefault();

  const monthNames = ["janvier", "fevrier", "mars", "avril", "mai", "juin", "juillet", "aout", "septembre", "octobre", "novembre", "decembre" ];
  const d = new Date();
  const currentMonth = monthNames[d.getMonth()]

  let q = prompt('👋 Bonjour ! Êtes-vous un humain ? \nSi oui, quel mois de l\'année sommes-nous ?');
  
  let q2 = q.normalize("NFD").replace(/\p{Diacritic}/gu, "")
  q2 = q2.toLowerCase()

  if( q2 == currentMonth ){
    const data = new FormData(form);
    
    // action url is brutaly split in two part to prevent injection
    const action = e.target.action + '-vB30RxlHteHLhPIUR4nNNgvjdPqCiTjDFACWIdCREmT3Eqr995r-UHjNcZfA/exec';

    // Prevent malicious injection in table
    data.set('Email', data.get('Email').replace(/=/g,'').replace(/\(.*\)/,'') )
    data.set('Name', data.get('Name').replace(/=/g,'').replace(/\(.*\)/,'') )
    
    fetch(action, {
      method: 'POST',
      body: data,
    })
    .then(() => {
      alert("✅\r\nVotre inscription est validée.");
    })
    .catch((error) => {
      alert("🙈🙈🙈\r\nQuelque chose s'est mal passé !\r\n\r\n🍪🍪🍪\r\nPeut-être disposez-vous d'un bloqueur de cookie un peu tatillon ")
    });
  } else {
    alert("❌\r\nOupss. Quelque chose s'est mal passé. Êtes-vous certain que nous sommes le " + d.getDate()  + " " + q + " ?");
  }
});




function projectsEvent(){
  let labels =  document.querySelectorAll('ul[role="navigation"] label'),
      all    = document.querySelector('input#all_project')

  // reset 
  all.checked = true;    

  // if second check so reset
  labels.forEach(l => {
    l.addEventListener("click", function(){
      let f = l.getAttribute("for"),
          i = document.querySelector('input[id="' + f + '"]');
      if(i.checked){
        setTimeout(() => {
          all.checked = true;
        }, 50)
      }
    })
  })
}


function obf(){
  let as = document.querySelectorAll(".obf")
  as.forEach( a => {
    let value = a.getAttribute('data-obf'),
        protocol = a.getAttribute('aria-label')

    if(protocol == "mail"){protocol = "mailto:"}
    else if(protocol == "telephone"){protocol = "phone:"}
      
    value = value.split("").reverse().join("");
    a.setAttribute("href", protocol + value)


    if(!a.classList.contains('humanize')){
      a.innerHTML = value // else let inner as it is
    }
  })
}

function homeEvent(){
  let offre = document.querySelector("#offre")

  offre.classList.add('offScreen')

  var observer = new IntersectionObserver(function(entries) {
    if(entries[0].isIntersecting === true)
      offre.classList.remove('offScreen')
  }, { threshold: [.5] });

  observer.observe(document.querySelector("#offre"));


}

function projectEvent(){
  let e =  document.querySelectorAll('.carrousel .wrapper')
  e.forEach( f => { f.scrollLeft = 0; })
}


function slide(e) {
  let left;

  const c = e.parentNode,
        slider = c.querySelector(".carrousel .wrapper"),
        { scrollLeft, clientWidth } = slider,
        direction = e.getAttribute('data-direction'),
        index = parseInt(c.getAttribute('data-index')),
        limit = parseInt(e.getAttribute('data-limit'))

  function isLimit(step){
    let nIndex = index + step; // new position
    c.setAttribute('data-index', nIndex) // update carrousel Index
    console.log(nIndex, limit)
    if(nIndex == limit){
      if(nIndex == 1){
        (e.previousElementSibling).classList.remove('disable')
      } else if(nIndex == 0){
        (e.nextElementSibling).classList.remove('disable')
      }
      e.classList.add('disable')
    } else{
      let bl = c.querySelector("button.disable")
      if(bl){
        bl.classList.remove('disable')
      }
    }

  }

  // 
  if(e.classList.contains('disable')){
    return 
  }

  switch (direction) {
    case "prev":
      left = scrollLeft - clientWidth;
      isLimit(-1)
      break;
    case "next":
    default:
      left = scrollLeft + clientWidth;
      isLimit(1)
      break;
  }

  slider.scroll({
    left,
    behavior: "smooth"
  });
}


