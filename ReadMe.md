# Site internet de Praxie Design

## Prévisualiser le site en local


1. Terminal
2. cd ${HOME}/Praxie\ Design\ Dropbox/PraxieDesign_Intern/Communication/Site_internet
3. hugo serve  --minify --cleanDestinationDir

_ou_

script `previsualiser_site.sh`


## Générer le site et le transférer le site sur le serveur

1. Terminal
2. Inscrire `cd ${HOME}/Praxie\ Design\ Dropbox/PraxieDesign_Intern/Communication/Site_internet`
3. Inscrire `hugo --minify --cleanDestinationDir`
4. Inscrire `lftp ftp://praxiea@ftp.cluster030.hosting.ovh.net:21/ -e "mirror --delete --reverse public www --exclude 'content/*/*/^.{0,32}\.(gif|jpe?g|bmp|png)$'; quit"`

_ou_

script `deployer_site.sh`


Dans les deux cas, il convient de renseigner son mot de passe sur le serveur.

*À savoir*

Le site est quoi qu'il en soit toujours généré sur notre ordinateur, dans un dossier nommé `public`
En cas de problème de synchronisation, il est toujours possible de copier le contenu de ce dossier `public` directement vers le dossier `www` du serveur — cela au moyen d'un client FTP comme Filezilla. 
[Voici une ressource externe](https://blog.lws-hosting.com/divers/filezilla-le-tutoriel-du-client-ftp-le-plus-populaire-pour-publier-son-site-web)


---


## Script automatique

Il existe un script 'deployer_site.sh' qui permet d'automatiser la génération et le transfert du site.

+ Un double clic suffit généralement à l'activer

*❗❗❗ Warning *

Lors de la première utilisation, le script peut nécéssiter une autorisation d'execution
[Apple fournit d'avantage de précision sur ce point](https://support.apple.com/fr-fr/guide/terminal/apdd100908f-06b3-4e63-8a87-32e71241bab4/mac)

1. Terminal
2. Inscrire `cd ${HOME}/Praxie\ Design\ Dropbox/PraxieDesign_Intern/Communication/Site_internet`
3. Inscrire `chmod 755 deployer_site.sh`

---

## Projets

### Organisation des projets

Les noms de projets commencent par :

+ 6 chiffres soit l'année et le mois du projet
+ un espace
+ un tiret
+ un espace
+ le nom du projet

### Créer un nouveau projet

Manuellement, en copiant un dossier existant et en mettant à jour les informations.

_ou_

Automatiquement, en cliquant sur le script `creer_projet.sh`.

_ou_

Dans un terminal avec `hugo new projets/"202206 - Nom de projet"/index.md`.

### Rédiger un projet

Un projet est avant tout un dossier.
Dans ce dossier se retrouve :

+ un fichier `index.md`
+ + divisé en un Avant-propos (frontmatter);
+ + et un contenu texte libre
+ des fichiers images et videos

L'avant-propos dans chaque fichier `index.md` est décisif. Il permet de renseigner des informations comme un titre, une date, une image de couverture, ...

Cet avant-propos se trouve toujours en début de chaque fiche, délimité à son début et sa fin par  trois tirets `---`


### Avant-propos spécifique au projet

	---
	title: "Titre du projet"
	auto_initié: false or 
	map: true
	details: 
	  Commanditaire: 
	    nom: 
	    nature: Remplacer la Nature du Commanditaire (Maître d’ouvrage, Soutien financier, ...). 
	  Equipe: Praxie Design
	  Budget: 
	  Date: {{ now.Year }}
	---
Titre 

	title: "Titre du projet"

Est-ce que le projet est exploratoire (picto praxie)

	auto_initié: true or false

Le projet doit-il être affiché sur le site ?

	map: true or false

Renseigner rapidement le commanditaire :

	details: 
	  Commanditaire: NOM DU COMMANDITAIRE

Renseigner le commanditaire et sa nature :

	details: 
	  Commanditaire: 
	    nom: NOM DU COMMANDITAIRE
	    nature: Remplacer la Nature du Commanditaire (Maître d’ouvrage, Soutien financier, ...). 

Qui a travaillé sur le projet

Une seul entité ?

	Equipe: Praxie Design

Plusieurs

	["Praxie Design", "Inddigo", "Ingérop"]

Date 

	Date: 2022


---

## Actu

### Organisation des actu

Les noms de projets commencent par :

+ 8 chiffres soit l'année, le mois, le jour de l'actu
+ un espace
+ un tiret
+ un espace
+ le nom de l'actu

### Créer une nouvelle actu

Automatiquement, en cliquant sur le script `creer_actu.sh`.

_ou_

Manuellement, en copiant un dossier existant et en mettant à jour les informations.

_ou_

Dans un terminal avec `hugo new actu/"20220612 - Nom de l'actu"/index.md`.

## Avant-propos spécifique aux actu

	---
	title: Praxie Design au micro de France Inter
	cover: 
	  file : cover.jpg
	date: 2022-08-30
	---

`file : cover.jpg` fait référence à la photographie de couverture de l'artcile sur les réseaux sociaux

---

## Rédiger un projet ou une actu


### Avant-propos

Chaque fiche projet commence par un avant-propos

### Contenu texte

Le texte se rédige en markdown (comme sur trello ou comme ce tutoriel)

#### Pour intégrer un lien

Dans le corps de texte :

	[TEXTE DU LIEN](https://praxiedesign.com)`

#### Pour intégrer une image

Pour intégrer une image :

1. Il convient de placer l'image dans le dossier du projet ou de l'actu
2. Puis, dans la fiche projet, intégrer l'image par l'expression suivante dans le corps de texte : `{{< img src="NOM_IMAGE.EXT" alt="TEXTE DE REMPLACEMENT DE L'IMAGE" >}}`


Les formats *jpg*, *png* ou *webp* sont parfait.

#### Pour intégrer une video

Le plus simple reste d'héberger la video sur _youtube_ ou _vimeo_.
Il convient ensuite d'intégrer le code Iframe de la video dans le corps de texte.

### Contenu image

Les images placées dans le répertoire du projet sont automatiquement mise en page dans le carrousel du projet.

La première image, dans l'ordre numéro-alphabétique, est automatiquement utilisée comme couverture de la page pour les réseaux sociaux.


---

## Ajouter des partenaires ou stagiaires

+ Ouvrir `equipe/index.md` avec un éditeur de texte
+ Modifier les lignes `present_members` et `past_members`
---

## Ajouter un logo de client sur la Home

+ ouvrir `contenu/client` avec le finder
+ Déposer le logo client en svg noir & blanc
