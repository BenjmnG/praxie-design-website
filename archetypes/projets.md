---
title: "{{ replaceRE "[[:digit:]]{6} - " "" .Name | humanize }}"
auto_initié: false
map: true
details: 
  Commanditaire: 
    nom: 
    nature: Remplacer la Nature du Commanditaire (Maître d’ouvrage, Soutien financier, ...). 
  Equipe: Praxie Design
  Budget: 
  Date: {{ now.Year }}
---
