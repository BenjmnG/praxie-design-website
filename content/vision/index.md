---
title: Notre vision
type: vision
cover: 
  file : cover.png
  alt : Description de la photo
partenaires: 
- - Le Cerema
  - https://www.cerema.fr/fr
- - Vélo & Territoires
  - https://www.velo-territoires.org/
- - La FUB
  - https://www.fub.fr/
- - Le Forum Vies Mobiles
  - https://forumviesmobiles.org/
- - Rue de l’Avenir
  - https://www.ruedelavenir.com/
- - CARA Active Mobility
  - https://www.cara.eu/fr/nos-filieres/mobilite-active-et-durable/
- - OXALIS, notre coopérative d'entrepreneurs
  - https://www.oxalis-scop.fr/
- - Le GEVIL, groupement des entreprises de Villeurbanne
  - https://www.gevil.fr/
- - Designers+, réseau de designers de la Région Auvergne-Rhône-Alpes
  - https://www.designersplus.fr/

description: 
---

> Rendre l’espace public accessible et convivial pour tous et favoriser les modes actifs
{.intro}

À la frontière entre le monde du design et celui de la santé, nous intervenons principalement sur le vélo et la marche à pied car ce sont de formidables moyens de répondre aux enjeux sociétaux actuels de santé et de dérèglement climatique.

Pour nous, l’espace public n’a de sens que s’il est adapté à tous et en premier lieu aux plus vulnérables : enfants et adolescents, personnes vieillissantes et âgées, personnes en situation de handicap moteur, sensoriel, cognitif, mental ou psychique, personnes en situation de précarité, personnes d'origine étrangère, non-francophones…

Nous aimons explorer de nouveaux territoires, challenger nos clients, apprendre en travaillant et travailler en rigolant. Nous sommes exigeants sur la qualité de notre travail et sur les impacts qu'auront nos réalisations dans la vie quotidienne des gens.

## Notre travail s'articule en 2 axes :

+ Des projets exploratoires, de type recherche-action ou création de contenu, que nous montons de toutes pièces, avec le soutien financier de partenaires engagés. Lors de ces projets, nous défrichons des terrains méconnus et nous participons à changer le regard de la société sur des sujets marginaux et pourtant positifs, pour l'humain et la planète. Ces projets nous permettent d'exprimer nos valeurs et d'acquérir de nouvelles connaissances et expertise, en nous confrontant aux usagers et à leurs diversité de besoins.
+ Des prestations de design, conseil et formation auprès de collectivités et d'entreprises. Ces projets, les liens que nous tissons avec nos clients et la découverte de leurs territoires, nous amènent souvent de nouveaux défis qui nourrissent notre imagination et notre volonté d'engagement pour de prochains projets exploratoires.


![](RetD_vs_presta_praxie_final.svg)


### Ce qui nous définit :

+ L’humain au centre
+ Travailler avec et pour les autres
+ L’optimisme et la bonne humeur
+ Tester, essayer, se tromper, recommencer, apprendre

***

Praxie Design est une entreprise coopérative au sein d'[Oxalis](https://www.oxalis-scop.fr)
Depuis plus de 20 ans, Oxalis contribue à créer une économie au service des personnes, en favorisant un développement raisonné et respectueux de l'environnement et des individus.
Oxalis nous offre le cadre pour entreprendre et pour rencontrer, échanger et travailler avec des personnes et des groupes qui partagent nos valeurs et contribuent à transformer le monde.
<br>
<br>
![logo oxalis](logo_oxalis.png)

***

# Notre manière de faire

## Être au plus proche du territoire

Chaque territoire, urbain ou rural, a ses particularités, ses acteurs, ses ressources, son histoire, sa géographie, qui demandent à être découverts et pris en compte. La compréhension du territoire, de l’éco-système, des relations entre chaque partie prenante est le point de départ de notre démarche. 

## Penser pour et avec les usagers

Pour concevoir les aménagements et services de demain, nous pensons pour les usagers dans leur diversité. Cette approche permet d’offrir une adéquation plus juste entre les besoins et les solutions proposées, et de fédérer les usagers autour d’un projet. Sur des principes de co-construction, notre volonté est de rendre les usagers acteurs du projet dont ils sont les principaux bénéficiaires.

## Prendre en compte les usagers les plus fragiles

En intégrant dans le processus de conception les besoins des plus fragiles, nous garantissons un confort d’usage et une sécurité optimale pour tous. Un parent devant manœuvrer une poussette ou un vélo-cargo, une personne âgée dont les capacités visuelles diminuent, une personne limitée dans sa capacité de mouvement ou de préhension… L’observation et la compréhension de ces besoins permet de concevoir des produits et services qui servent au plus grand nombre, quelles que soient leurs capacités.

## Concevoir de manière responsable

Notre volonté est de créer des services et des produits qui ont une empreinte environnementale la plus faible possible (matériaux, processus de fabrication, choix d’entreprise locale). Pour nous, cette démarche éco-responsable est source d’inspiration et d’innovations.

