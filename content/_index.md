---
type: index
title: Conseil et Innovation en mobilité active et inclusive
baseline: Rendre l’espace public accessible et convivial pour tous et favoriser les modes actifs
vision: "Au moyen des outils du _design_, nous intervenons dans le domaine de la _mobilité active_, principalement vélo et marche à pied, car c’est un formidable moyen de répondre aux enjeux sociétaux actuels de _santé_ publique et individuelle et de dérèglement climatique.<br><br>_Nous œuvrons pour rendre l’espace public adapté à tous et en premier lieu aux plus vulnérables._"
offre:
  - 
    service: Conseil
    description: Conseil aux collectivités et aux entreprises sur leur politique de mobilité active & inclusive
  - 
    service: Design
    description: "Conception de&#8239;produits et&#8239;services de&#8239;mobilité active &&#8239;inclusive

      <br><br>


    	+ Design de services

			+ Design produits et mobilier

			+ Branding et identités visuelles
      
			+ Systèmes signalétiques

      "
  - 
    service: Formation
    description: Formation et animation en mobilité active & inclusive
reclame: '<img src="./svg/Talents_velo_2022-White.svg" aria-hidden="true" aria-label="Logo Lauréat des Talents du vélo 2022"> <p>Lauréat des Talents du vélo 2022 pour le court métrage Les Roues du Possible</p>'
---

