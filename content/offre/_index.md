---
type: offre
title: Notre offre
description: 

---

À travers des interventions très variées, nous accompagnons les entreprises et les collectivités souhaitant développer la marche et le vélo en tant que moyens de déplacement et améliorer les espaces publics et leurs usages.
Convaincus que les mobilités actives sont une partie de la solution pour permettre à chacun et chacune de vivre mieux, en meilleure santé et de manière plus autonome, nous imaginons, concevons et expérimentons de nouveaux services et équipements, accessibles au plus grand nombre.
Notre action se structure en trois volets : Conseil, Design, Formation.