---
ordre: 3
nature: Formation
title: Sensibiliser et accompagner aux enjeux de mobilité de demain
projets: 
  - nom: Formation Adma
    link: "/projets/202107-adma/"
methodes: 
  -
    titre: Formation
    texte: "Nous formons les professionnels à la mobilité active et inclusive. Le contenu est créé selon le public : professionnels médico-sociaux, urbanistes, éducateurs mobilité, techniciens, élus, toute l'équipe d'un service, d'une collectivité... Les formats proposés sont adaptés à vos besoins : interventions courtes et ponctuelles ou cycles de formation plus longs. Notre approche pédagogique est axée sur la pratique et l’expérience. 
Nous avons formé les experts ADMA, de l’Académie des Mobilités Actives. Nous formons des ergothérapeutes, des éducateurs Mobilité à Vélo, des designers, des urbanistes... "
  -
    titre: Animation d’événements
    texte: "Nous concevons et animons des événements sur le thème de la mobilité active. Que ce soit de la sensibilisation auprès du grand public, des essais de vélos adaptés à des besoins particuliers, des jeux avec des outils de pédagogie ludiques ou des ateliers de co-conception, ces événements sont un moyen de toucher directement les usagers et de comprendre leurs besoins."
---

Faire changer les comportements passe aussi par la sensibilisation et la formation du grand public. 
Former des professionnels ou des décideurs au design actif et inclusif permet d'envisager la création de services et d'infrastructures plus inclusifs et plus qualitatifs pour tous. 

En immersion ou en atelier, nos expériences sur le terrain nous permettent d’aller régulièrement à la rencontre des usagers. faire tester des vélos adaptés, comprendre les besoins nous permet de donner du sens et du relief à des enjeux complexes.
