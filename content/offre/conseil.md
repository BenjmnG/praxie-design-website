---
ordre: 1
nature: Conseil
title: À l'écoute des usagers et aux côtés de nos partenaires
projets: 
  - nom: Ile-de-France Mobilités
    link: "/projets/202001-idfm-aide-a-l-achat/"

methodes: 
  -
    titre: Compréhension et identification des besoins
    texte: "Un piéton, une cycliste, un automobiliste, une conductrice de bus. Ils possèdent tous une expertise du quotidien, riche et précieuse. Afin d’identifier les attentes, les envies et les besoins de chacun, nous utilisons plusieurs outils :

      + Reconnaissance terrain

      + Ateliers participatifs

      + Entretiens qualitatifs

      + Questionnaires en ligne
      
      + Personas et parcours usagers
      "
  -
    titre: Activation des écosystèmes
    texte: "En nous appuyant sur les structures existantes (administrations, entreprises, associations) et les acteurs locaux, nous réunissons les forces vives du territoire pour favoriser l’intelligence collective, ce qui nous permet de faire des propositions pour le bien de tous. L'implication de ces acteurs locaux dès les prémices du projet nous permet d'assurer la durabilité de la dynamique créée sur le territoire durant notre mission, au-delà de cette dernière."
  -
    titre: Assistance à maîtrise d’ouvrage (AMO)
    texte: "Notre expérience dans le milieu du design nous permet d’accompagner nos clients afin de choisir les prestataires les plus compétents pour produire des éléments de signalétique, des supports de communication papier/numérique, du mobilier, des services… Nous proposons aussi un suivi de la mise en place de nos préconisations (travaux d'infrastructures, installation de la signalétique et du mobilier, etc.) afin d’assurer un bon usage et que les besoins des divers usagers soient respectés."
  -
    titre: Assistance à maîtrise d’usage (AMU)
    texte: "Nous accompagnons le maître d’ouvrage et/ou maître d’œuvre à chaque étape du projet pour garantir que ce dernier assure une qualité d’usage optimal, notamment pour les usagers les plus fragiles, grâce à notre connaissance fine des besoins des usagers dans leur diversité. Suivant le contexte du projet, nous proposons parfois d’intégrer les usagers dans toutes les phases du projet et auprès de l’ensemble des parties prenantes. Des usagers-acteurs, experts de leur quotidien et de leurs attentes et qui sont avant tout les utilisateurs finaux du projet."
  -
    titre: Accessibilité et inclusion
    texte: "
    Au-delà de notre expertise sur les questions d'accessibilité des espaces publics pour les usagers dans leur diversité de capacités et de besoins, nous avons une connaissance pointue des différents vélos et autres engins innovants disponibles sur le marché européen et de leurs usages par les personnes à mobilité réduite et les personnes vieillissantes. En effet, il existe beaucoup de solutions pour permettre à chacun, en fonction de ses capacités, de se déplacer de manière active.

    Nous accompagnons les entreprises, les collectivités, les acteurs du tourisme sur les points suivants, par exemple :

    + Accessibilité des espaces publics, pour les piétons et cyclistes à mobilité réduite

    + Labellisation Tourisme & Handicap d’une voie verte, d’une véloroute…

    + Conception de vélos adaptés, ergonomiques, pour enfant, adulte, personne âgée

    + Choix de parc vélo (pour enfants, séniors, personnes en situation de handicap, familles…),

    + Design et mise en place de services de partage ou location de vélos (avec choix des prestataires), formation des acteurs locaux qui assureront le service.
    "
---

Comme un préalable à toute action, Praxie Design se fait fort de conseiller les collectivités et les entreprises sur leurs politiques de mobilité actives et inclusives. Notre expertise, affûtée par des années de pratiques des villes et des territoires à pied et à vélo, se fonde sur notre expérience de terrain et une maîtrise des enjeux d’accessibilité et d’inclusion.
