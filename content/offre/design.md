---
ordre: 2
nature: Design
title: Le design, un formidable outil au service des mobilités actives et inclusives.
projets: 
  - nom: Chronovélo
    link: "/projets/201706-chronovelo/"
  - nom: Grésivaudan
    link: "/projets/202204-gresivaudan/"

methodes: 
  -
    titre: Veille créative et benchmark
    texte: "Nous avons une pratique de veille constante qui nous permet d’intervenir sur vos projets avec une connaissance des pratiques et des innovations dans le monde entier. Nous proposons la conception de cahier de veille spécifique sur une thématique particulière. Cet outil permet de sensibiliser les parties prenantes, de stimuler l’imagination et les échanges en phase créative."
  -
    titre: Création de mobilier adapté
    texte: "Nous créons du mobilier spécifique ou adaptons un mobilier « catalogue » afin de répondre au mieux aux besoins des usagers et à l’identité de votre marque ou de votre territoire. Nous avons l’habitude de créer du mobilier robuste qui résiste aussi bien aux contraintes urbaines que rurales. Nous travaillons avec le tissu de PME locales reconnues pour leur savoir-faire et leur esprit d’innovation."
  -
    titre: Design thinking et conception des nouveaux services
    texte: "Le design thinking est une stratégie d’innovation qui remet l’humain au cœur des projets. Cette forme d’innovation tente de comprendre l’humain sous toutes ses coutures (ses modes de vies, ses usages, ses motivations, ses imaginaires et ses émotions) pour découvrir de nouvelles opportunités (problèmes ou besoins mal ou non-adressés, opportunités de création de confort ou plaisir) et concevoir des produits, des services ou des organisations proposant des expériences nouvelles."
  -
    titre: Création d’identité
    texte: "Le design d’identité commence par définir des enjeux, affirmer une vision et des valeurs qui serviront de lignes directrices tout au long du projet. Nous créons des identités fortes autour des projets que nous accompagnons, des images porteuses de sens et facilement identifiables par les parties prenantes (commanditaires, partenaires et bien sûr les usagers).

      Pour cela, nous sommes en mesure de vous accompagner sur les points suivants :

        + Plateforme de marque

        + Création de nom

        + Conception de logos et d’univers graphiques

      Appliqué à l’espace public, le design d’identité contribue à le démarquer et permet de tisser un lien émotionnel avec les usagers. Non seulement il les aide à identifier les aménagements et les services, mais il raconte une histoire et contribue à améliorer l’expérience vécue."
  -
    titre: Rendre l’espace public lisible et accessible
    texte: "Le système signalétique est à la fois un outil stratégique et un ensemble de mesures et d’objets visuels dans l’espace pour permettre aux usagers de s’orienter et s’informer. Il doit être simple et intuitif et sa mise en place comprend 3 étapes :

      + Analyse des informations pour orienter et informer les usagers
      
      + Conception d’un système graphique identitaire et fédérateur propre au contexte

      + Définition des éléments signalétiques (mobiliers signalétiques et recommandations précises sur les supports, les informations et leur localisation)"
  -
    titre: Communication et sensibilisation
    texte: "Notre expertise en design graphique et design de communication nous permet de réaliser des outils opérationnels pour communiquer et sensibiliser :

      + Plaquettes, flyers, affiches

      + Site internet et visuels pour campagne web marketing

      + Applications et webapp"

---

Nous concevons des produits et des services de mobilité active et inclusive : identité globale de réseau cyclable ou piéton, système signalétique ou services innovants pour faciliter la vie des usagers.

Notre approche est centrée sur les usages et se base sur les méthodes du design : étude de cas, cahier de veille, carte cognitive, atelier de conception...

Notre objectif est de rendre l’espace public plus lisible et accessible.
Nous retrouvons d’ailleurs ces deux valeurs dans les critères qui guident chacune de nos interventions tout comme celle d’efficacité, de pragmatisme, de sécurité et évidemment de convivialité.

Par le design, nous avons à cœur d’œuvrer au service de la santé et d’une société plus inclusive et nous pouvons faire émerger des besoins jusqu’alors encore tus, montrer des choses nouvelles, répondre à des besoins spécifiques.

À cela, n’oublions pas l’envie de satisfaire des critères esthétiques car ils demeurent un formidable levier pour créer un lien émotionnel, attiser la compréhension et l’appropriation d’un projet par le public — et donc faire changer les comportements !
