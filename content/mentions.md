---
map: false
type: mentions
title: Mentions légales
---

## Éditeur du site

Praxie Design  
[bonjour@praxiedesign.com](mailto:bonjour@praxiedesign.com)  
61 cours de la République,  
69100 Villeurbanne

## Siège social

Praxie Design est une entreprise coopérative au sein d’Oxalis  
Oxalis SCOP SA  
603 boulevard Président Wilson  
73100 Aix-les-Bains  
Siren : 410 829 477  
Inscrite au Registre du Commerce et des Sociétés de Chambéry  
sous le n° 410 829 477

## Hébergeur

SAS OVH  
2 rue Kellermann  
BP 80157  
59100 Roubaix  
Support Technique : 08.99.49.87.65  
Email: [support@ovh.com](mailto:support@ovh.com)  
Site : [www.ovh.com](http://www.pauline-desgrandchamp.com/www.ovh.com)  
Fax : 03.20.20.09.58

## Informations personnelles collectées

Praxie Design ne collecte pas de données sur les utilisateurs du site.

Dans le cadre de notre newsletter, nous veillons à ne demander que des données strictement nécessaires, soit une adresse email.

### Rectification des informations nominatives collectées

Conformément aux dispositions de l’article 34 de la loi n° 48-87 du 6 janvier 1978, l’utilisateur dispose d’un droit de modification des données nominatives collectées le concernant.

+ par l’envoi d’un courrier électronique
+ par l’activation du lien de désinscription inclus dans la newsletter

La modification interviendra dans des délais raisonnables à compter de la réception de la demande de l’utilisateur.

---

Ce site a été conçu avec, au cœur, l'idéal d'un web accessible et sobre en énergie.