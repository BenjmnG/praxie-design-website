---
title: Benoit Fournier-Mottet
role: Designer eco-conception
statut: partenaire
contact:
  -
    - website
    - https://beodesign.fr/
  -
    - mail
    - benoitfm@beodesign.fr
actif: true
supervitus305: true
---
