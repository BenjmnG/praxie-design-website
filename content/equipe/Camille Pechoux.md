---
title: Camille Pechoux
role: Consultante en design actif et santé. Co-gérante de Praxie Design
statut: cogérant
actif: true
supervitus305: true
contact:
  -
    - linkedin
    - https://www.linkedin.com/in/camillepechoux/

---
Dans ma pratique clinique d’ergothérapeute visant à accompagner l’indépendance et l’autonomie des personnes ayant des besoins particuliers, j’ai pu constater à quel point l’environnement, lorsqu’il n’est pas accessible, pouvait être un énorme frein à cette (re)conquête de l’autonomie. Aussi, j’ai décidé d’œuvrer pour l’accessibilité des espaces publics afin d’avoir une action plus globale. Par cette entrée, j’ai pu analyser les types de mobilité proposés aux publics dits “à mobilité réduite”.
Passionnée par la mobilité à vélo, je me suis alors spécialisée, en 2011, sur l’accès à ce moyen de déplacement pour les personnes ayant des besoins particuliers : les enfants et adultes en situation de handicap et les personnes âgées, mais également toutes les personnes dites “à mobilité réduite” (femmes enceintes, parent avec jeunes enfants, enfants, personnes non francophones, touristes, personnes chargées, pressées, stressées…).