---
title: Équipe
type: equipe
intro: 
  generale: Nous nous sommes rencontrés en 2015 avec la création du collectif [SuperVitus305](/historique). En 2020, Nous créons Praxie Design, au sein de la coopérative Oxalis, avec le désir de travailler ensemble en associant nos compétences et notre créativité. Nos parcours sont très différents, nos expériences multiples. C’est ce qui fait notre force.  

  partenaire: Nous avons monté une équipe de professionnels du design et métiers associés, aux compétences spécifiques et complémentaires, coopérant ensemble depuis plusieurs années. Nous sommes experts dans nos domaines de compétences respectifs mais, avant tout, nous sommes des usagers réguliers du vélo et de la marche, et des êtres humains aimant les rencontres et les expériences que l'on peut faire dans les espaces publics.

description:
present_members:
past_members:
  - nom : Charlie Fievet
    role: Urbaniste / Ergothérapeute _(en stage)_
    link: 
  - nom : Sirine Mehigueni
    role: Aide au développement _(en stage)_
    link: 
  - nom : Johan Guerin
    role: Designer _(en stage)_
    link: 
---
