---
title: Thomas Rosset
role: Designer.   Co-gérant de Praxie Design
statut: cogérant
actif: true
supervitus305: true
contact:
  -
    - linkedin
    - https://www.linkedin.com/in/thomasrosset/

---

Designer et enseignant en design, j’ai commencé ma carrière dans le domaine de l’écologie scientifique, ce qui m’a très vite amené à me questionner sur nos modes de vies et leurs impacts sur l’environnement.
J’ai travaillé pendant dix ans aux États-Unis en tant que designer graphique. De retour en France, j’ai créé un studio de design d’identité au service de projets innovants et responsables. Ma pratique s’est ensuite ouverte à une approche plus globale du design, centrée sur les usages et les besoins des usagers.
J’utilise le vélo depuis longtemps, pour mes déplacements quotidiens et pour mes loisirs, en France comme à l’étranger. J’ai assisté (et parfois accompagné) à toutes les évolutions de la pratique du vélo. Pour moi, le vélo est plus qu’un mode de déplacement, c’est un moyen d’explorer et de vivre pleinement le monde qui nous entoure.