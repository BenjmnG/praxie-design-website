---
map: false
title: __À l'origine__ SuperVitus 305
cover: supervictus.jpg
cover: 
  file : 11_cover-supervictus.jpg
  alt : L'équipe de supervictus en repérage dans les rue de Grenoble
---

SuperVitus305 est un collectif d’experts indépendants qui ont été associés entre 2016 et 2021&nbsp;:
+ 5 années pendant lesquelles nous avons accompagné des collectivités dans la mise en œuvre de projets innovants : infrastructure, itinéraire vélo, réseau structurant vélo, voie verte…
+ 5 ans de projets motivants pendant lesquels nous sommes devenus experts en mobilité active en plus de nos compétences propres.
+ 5 années passionnantes et riches en émotions.











