---
title: contact
type: contact
boogie: "Praxie Design a fondé Boogie. Boogie est un espace de travail coopératif autour des mobilités actives, durables & de la transition écologique, situé à Villeurbanne (69), regroupant différents acteurs (entreprises, associations, fédérations)."
---
