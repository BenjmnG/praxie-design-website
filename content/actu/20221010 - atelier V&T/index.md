---
title: Designer des aménagements cyclable accessibles à tous 
cover: 
  file : cover.jpg
date: 2022-10-10
---

Clap de fin sur les 26e Rencontres Vélo & Territoires à Bourges. Plein de belles rencontres et une occasion pour nous de partager notre expérience à travers l’atelier « Designer des aménagements cyclable accessibles à tous », que nous avons conçu et co-animé devant 80 personnes.

{{< img src="cover.jpg" alt="Un groupe de participant travaillant ensemble sur une cartographie" >}}

Pour cet atelier participatif, nous avons imaginé un jeu qui engage les participants à la fois dans la démarche de conception universelle et dans l’approche d’empathie pour les usagers, qui constituent la base de notre travail.

Nous espérons que cette expérience aura contribué à déconstruire les préjugés et les idées reçues sur l'accessibilité, et à montrer que les notions d'accessibilité et d'attractivité sont indissociables dans le cadre de la conception d’infrastructures de mobilités actives.

Merci à Marion Ailloud, Thomas Jouannot, Flavien Lopez, Nicolas Amblard et Johanne Collet du Cerema, Camille Gaumont et Olivier BALAGUIER de l’Académie des Mobilités Actives (ADMA) pour leur contribution et l’animation.

Un grand merci à l’équipe de Vélo & Territoires pour leur confiance renouvelée et notamment à Noémie Rousset pour son rôle dans la coordination et l’animation.