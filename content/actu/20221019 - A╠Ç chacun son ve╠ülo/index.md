---
title: "Budget participatif : Villeurbannaise, Villeurbannais à vos votes !"
cover: 
  file : cover.png
date: 2022-10-19T09:23:18+02:00
---

Villeurbannaise, villeurbannais,  
Chère Voisine, Chère Voisin,
Chers partenaires,  

<br>

L'automne arrive, les feuilles tombent, mais les projets fleurissent.
En effet, nous souhaitons vous parler d'À chacun son vélo, initiative que nous avons déposée dans le cadre du budget participatif de la ville de Villeurbanne.

<iframe src="https://player.vimeo.com/video/761752410?h=721b24a61d&title=0&byline=0&portrait=0" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

_À chacun son vélo_ part du constat que se déplacer à Villeurbanne à vélo est de plus en plus désirable et encouragé. Pourtant, certaines personnes ne le peuvent pas, pour des raisons de santé notamment.

<br>

Vous vous en souvenez peut-être : en 2021, nous avions mené l'expérimentation SAMVA, un centre de conseil et d'essai pour les personnes qui ne pouvaient pas faire du vélo classique. Ce fut un succès et cela a fait émerger le besoin des personnes qui ne pensaient pas pouvoir faire du vélo. Cependant, la difficulté à acquérir un vélo adapté à ses besoins a été mise en lumière par ces accompagnements. Aussi, aujourd'hui, nous proposons que le budget participatif permette l'acquisition et la location de vélos adaptés aux capacités de chacun : bicyclette facile à enjamber, tricycle, handbike, tandem, triporteur pour fauteuil roulant apporteront une réponse à chaque villeurbannais !

<br>

Vous trouverez un résumé vidéo notre projet ici : Praxie Design
Nous comptons sur votre soutien et votre vote.

<br>

[Projet 981 - À chacun son vélo](https://participez.villeurbanne.fr/processes/budgetparticipatif2022/f/73/budgets/3/projects/80)

<br>

Il est nécessaire de voter pour 3 projets minimum : aussi, si vous ne savez pas quel autre projet soutenir, nous vous conseillons les projets suivants : 
[n°995 - La Forêt des Alytes](https://participez.villeurbanne.fr/processes/budgetparticipatif2022/f/73/budgets/3/projects/193) et[ n°1108 - Vélo-école pour tous](https://participez.villeurbanne.fr/processes/budgetparticipatif2022/f/73/budgets/3/projects/200)

<br>

Le vote est ouvert à toutes les personnes de plus de 7 ans vivant, étudiant, ou travaillant à Villeurbanne.

<br>

Merci pour votre soutien !