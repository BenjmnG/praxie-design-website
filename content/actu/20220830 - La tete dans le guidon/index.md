---
title: Praxie Design au micro de France Inter
cover: 
  file : cover.jpg
date: 2022-08-30
---

Zoom sur la pratique du vélo possible par toutes et tous sur France Inter ce matin ! Nous avons eu la chance d'accompagner le journaliste Rémi Brancato pour le casting de cette courte émission mettant en valeur une de nos missions : accompagner le développement de la pratique du vélo au quotidien par tous, y compris par les personnes ayant des besoins particuliers.

Un grand merci à Dominique pour son témoignage, suite à l'accompagnement dont il a bénéficié dans le cadre de [notre expérimentation d'un centre de conseils et d'essais de vélos adaptés](/projets/202105-samva), projet qui a été soutenu par la Métropole de Lyon et Malakoff Humanis.
Merci aussi à Catherine et à Joseph Mignozzi, fondateur de Benur, à qui Rémi a tendu le micro, dans les rues de Villeurbanne et dans nos locaux partagés, au [Boogie ](https://boogie.praxiedesign.com).

Praxie Design accompagne les collectivités et les entreprises audacieuses, afin que la pratique de la marche et du vélo soit possible, confortable et sécure par les usagers dans leur diversité (enfants, personnes âgées, personnes en situation de handicap, femmes, personnes en insertion... et tout un chacun !), par du design de services, du design d'identité, de la stratégie de communication, du conseil pour les aménagements et infrastructures, de la formation, des expérimentations...
   
<br>

Pour écouter : [La tête dans le guidon : Quand le vélo redonne des jambes](https://www.radiofrance.fr/franceinter/podcasts/le-zoom-de-la-redaction/le-zoom-de-la-redaction-du-vendredi-22-juillet-2022-9933590)
{.goto}
