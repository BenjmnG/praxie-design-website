---
title: "Talents du vélo 2022"
cover: 
  file : cover.png
date: 2022-12-14
---



Praxie Design est lauréat des Talents du vélo 2022 pour le court métrage [Les roues du possible](https://lnkd.in/eP3QnAq2) que nous avons réalisé avec [Marc Aderghal](https://www.linkedin.com/in/marcaderghal/).

{{< img src="cover.jpg" alt="remise du prix Talents du vélo 2022 le 13 décembre 2022, à la mairie de Paris IX" >}}

Merci au [Club des villes et territoires cyclables et marchables](https://villes-cyclables.org/) pour ce prix et pour l’organisation de cet événement.

<br>

Nous sommes contents de voir que la question de l’inclusion dans le monde du vélo commence à être prise au sérieux car elle nous anime au quotidien dans notre travail chez Praxie Design. Que ce soit dans le conseil auprès des collectivités ou le design de service, d’identités visuelles, de signalétique ou de mobilier, nous mettons tout en œuvre pour rendre le système vélo et marche adapté à tous, quelles que soient ses capacités.

<br>

Les roues du possible devient une série. 

<br>

Le premier volet, qui a été primé, évoque la pratique du vélo lorsqu’on est en situation de handicap. Le second épisode, en cours de réalisation, laisse la parole à des personnes âgées qui témoignent de leur pratique du vélo. Et d’autres sont dans les tuyaux…

<br>

Nous tenons aussi à remercier [Malakoff Humanis](https://www.malakoffhumanis.com/) qui nous accompagne dans cette aventure et qui, par son soutien financier, la rend possible.

<br>

[Retrouvez tous les lauréats](https://villes-cyclables.org/-venements/les-talents-du-velo-2022/les-huit-laureats-2022-des-talents-du-velo) sur le site du Club des villes et territoires cyclables et marchables




