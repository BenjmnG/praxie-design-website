---
title: Réalisation d’un gabarit de RIS pour la V50
map: true
details: 
  Commanditaire: 
    nom: Comité d’itinéraire de la V50 - La Voie Bleue
    nature: Maître d’ouvrage
  Equipe: Praxie Design
  Budget: 1000
  Date: 2021
---

En s’appuyant sur sa connaissance de la V50, Praxie Design a conçu et réalisé un gabarit de RIS.
{.intro }

L’objectif de cette mission a été de fournir aux gestionnaires de la V50 un outil graphique adapté et modulaire qui permette, en fonction des besoins de chacun, de réaliser un panneau d’informations ou RIS (Relais Information Service) sur leur territoire.
Une gamme complète de pictogrammes a été réalisée dans le cadre de cette mission.