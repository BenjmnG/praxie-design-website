---
title: Stratégie mobilité et aménagement d’espace
map: true
details: 
  Commanditaire: 
    nom: CEA (Commissariat à l’énergie atomique et aux énergies alternatives)
    nature: Maître d’ouvrage
  Equipe: Praxie Design et Kevin Kristen Studio
  Budget: 25000
  Date: 2020 — 2021
---

Nous avons accompagné le CEA dans sa stratégie mobilité et dans l’aménagement du site de Grenoble, véritable « ville dans la ville ». Le centre de recherche CEA-Grenoble s’est construit au fil du temps sans réelle cohérence et connait actuellement des problèmes d’accidentologie justifiant une réflexion globale.
{.intro}

L’objectif de cette mission a été double : 

1. Faire évoluer le site vers un espace public apaisé, sécurisé et tourné vers les usages et les déplacements actifs (piétons, vélos, etc.). 
2. Créer une véritable cohérence et une identité au site. 

Nous avons proposé une réponse en 3 volets permettant au client d’avoir à la fois une vision globale de l’existant et du champ des possibles, avec des solutions concrètes :

+ A partir d’un repérage terrain, conception d'un diagnostic du site actuel qui a permis d’identifier les problèmes et conflits d’usage présents sur le site.   [60 pages avec plus 100 points relevés et analysés]
+ Un cahier de veille de pratiques et de projets réalisés à travers le monde, choisis pour leur pertinence au regard des enjeux de la mission, et qui permettent d’imaginer des solutions et d’échanger.   [40 pages, 43 projets illustrés]
+ Des préconisations de principes généraux et de réalisations concrètes sous forme de fiches actions.   [70 pages et 10 fiches action]
