---
title: Véloroute du futur
map: true
details: 
  Commanditaire: 
    nom: Région Auvergne-Rhône-Alpes
    nature: Maître d’ouvrage  
  Equipe: "[SuperVitus305](/historique)"
  Montant de la mission: 100000
  Date: 2019 — 2021
---

[SuperVitus305](/historique) a imaginé un itinéraire attractif et inclusif au fil de l’eau, offrant aux usagers une expérience singulière à travers un ensemble d’innovations qui pourront être testés et éprouvés in situ.
{.intro}

En 2017, la Région Auvergne-Rhône-Alpes a identifié un tronçon de la ViaRhôna, entre Ampuis et Condrieu, sur lequel elle désire expérimenter, au fil des années, des innovations en matière d’infrastructure, de services et de scénarisation du patrimoine. Conçu comme un démonstrateur, il pourra être pris en exemple pour des réalisations futures sur d’autres itinéraires.
Dans une mission d’assistance à maîtrise d’ouvrage, [SuperVitus305](/historique) a imaginé la véloroute de demain à travers un scénario d’itinéraire expérientiel et inclusif — un espace flexible et innovant de huit kilomètres au fil de l’eau, où cyclistes et piétons s’entrecroisent pour se déplacer, flâner, se reposer, jouer ou découvrir le territoire.

À la manière d’une partition, la scénarisation superpose les différentes dimensions du parcours de la véloroute du futur pour créer une expérience singulière pour les usagers, quels qu’ils soient. Pour mener à bien sa mission stratégique, notre studio a mobilisé plusieurs outils issus des méthodologies du design, en particulier la veille créative et la cartographie des usages.