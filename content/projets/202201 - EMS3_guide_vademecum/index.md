---
title: "Livre blanc sur le stationnement vélo"
auto_initié: false
map: true
details: 
  Commanditaire: 
    nom: Eurométropole de Strasbourg
    nature: Maître d’ouvrage
  Equipe: Inddigo, Kevin Kristen, Praxie Design et Artgineering
  Budget: 
  Date: 2022
---

Le livre blanc « Stationner son vélo en toute sécurité », est une publication de la Ville et Eurométropole de Strasbourg à laquelle nous avons participée.
{.intro }

D’une soixantaine de pages, à l’attention des bailleurs, cet ouvrage à pour objectif de sensibiliser et d'informer sur les bonnes pratiques en terme de conception et de construction de stationnement vélo dans les bâtiments.

A la différence des autres guides sur le sujet, cette publication met l’accent sur la démarche et la méthodologie nécessaire à mettre en œuvre en amont de la construction, pour bien comprendre les besoins et les attentes des usagers auxquels ces espaces de stationnement doivent répondre.

Nous avons accompagné le groupement sur la structuration et la rédaction du contenu, ainsi que la conception des illustrations. Et bien sûr, nous avons veillé à ce que les futures offres de stationnement soient réellement pensées avec un confort maximum pour toutes les personnes qui pédalent, quels que soient leur profil, leur type de vélo et leurs usages.

- Direction de publication : Ville et Eurométropole de Strasbourg
- Rédaction : Kevin Kristen et ‌Inddigo, en collaboration avec Pierre-Marie GARNIER, Praxie Design et Artgineering
- Design graphique : Kevin Kristen
- Illustrations : Kevin Kristen ‌et Praxie Design