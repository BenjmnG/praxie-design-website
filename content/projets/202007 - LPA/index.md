---
title: Conseil et préconisations pour l'aménagement d'un espace vélo
auto_initié: false
map: true
details: 
  Commanditaire: 
    nom: LPA (Lyon Parc Auto) 
    nature: Maître d’ouvrage
  Equipe: ["[SuperVitus305](/historique)"]
  Budget: 
  Date: 2020
---

En 2020, LPA prend la mesure des besoins croissants de services adaptés pour les usagers du vélo et plus précisément de stationnements vélo sécurisés. Dans ce contexte, la mission est confiée à [SuperVitus305](/historique) de faire des préconisations pour améliorer les usages du parc de stationnement vélos et vélos cargo de Cordeliers.
{.intro}

Pour cela, nous avons fait un diagnostic de l'existant et une série de 7 préconisations argumentées et illustrées d'exemples précis pour permettre à tous les usagers du vélo, quels que soient leurs capacités et le vélo qu'ils utilisent, de le stationner de manière sécurisée et dans des conditions d'usage optimum.