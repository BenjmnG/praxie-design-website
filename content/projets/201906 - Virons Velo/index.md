---
title: Virons Vélo, une offre vélo famille
map: true
details: 
  Commanditaire: 
    nom: CC des Monts du Pilat
    nature: Maître d’ouvrage
  Equipe: "[SuperVitus305](/historique)"
  Budget: 23000
  Date: 2018 — 2020 
---

Conception d'une offre vélo famille, comprenant des itinéraires autour de la commune de Marlhes dans le Parc naturel régional du Pilat.
{.intro}

[SuperVitus305](/historique) a été choisi pour concevoir et réaliser une offre vélo famille sur la commune de Marlhes. Pour cela, nous avons développé des méthodes de travail collaboratives et participatives pour englober tous les acteurs concernés via des ateliers incluant des usagers, des élus, des habitants afin de répondre aux attentes et besoins des futurs utilisateurs de ces itinéraires.

Pour faire découvrir ce territoire au patrimoine riche, un travail pour scénariser le parcours a été mené au printemps 2018 afin de construire un récit tout au long de l’itinéraire pour éveiller la curiosité de tous et notamment des familles.

L’équipe de [SuperVitus305](/historique) a créé une charte graphique, du mobilier dédié pour l’itinéraire et a mis en place une application numérique simple et intuitive qui permet de se géolocaliser sur les parcours et d’accéder à du contenu : informations touristiques et notamment des témoignages audio d’habitants.
