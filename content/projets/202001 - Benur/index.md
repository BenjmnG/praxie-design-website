---
title: Accompagnement pour la création d'un vélo inclusif 
map: true
details: 
  Commanditaire: Benur  
  Équipe: Praxie Design
  Date: 2016 — 2020
---

Praxie Design a accompagné l’équipe Benur dans la conception d’un service de vélo accessible à tous.
{.intro}

Praxie Design a accompagné le design produit du vélo afin qu’il réponde au mieux aux besoins des publics cibles (personnes en fauteuil roulant et personnes ayant des difficultés de marche). Pour cela, les outils d’ergonomie ont été appliqués (cahier des charges fonctionnels, évaluation du produit, accompagnement des équipes), associés à notre fine connaissance des besoins des usagers à mobilité réduite et de la pratique du handbike.
Praxie Design a également contribué au design du service de mobilité Benur dans les territoires.

## Notre apport :

+ Ergonomie liée à la pratique du vélo par les personnes à mobilité réduite
+ Tests usagers
+ Mise en réseau, ouverture sur l’écosystème vélo 
+ Design de services adapté aux territoires et aux écosystèmes locaux
+ Stratégie
