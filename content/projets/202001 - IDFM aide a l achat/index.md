---
title: Etude pour une nouvelle aide à l’achat de vélos 
map: true
details: 
  Commanditaire: 
    nom: Ile-de-France Mobilités
    nature: Maître d’ouvrage
  Equipe: Praxie Design
  Budget: 15 100 € HT puis 4 700 € HT
  Date: 2020 — 2021
---

Afin d’accompagner Ile-de-France Mobilités dans la définition de leur nouvelle aide à l’achat de vélo pour les personnes à mobilité réduite, Praxie Design a réalisé en 2020 une étude approfondie sur les vélos adaptés et les aides financières possibles.
{.intro}

En 2021, Praxie Design a poursuivi cet accompagnement, en concevant et animant des ateliers destinés aux acteurs de l’écosystème francilien en lien avec les personnes à mobilité réduite, afin de promouvoir la mobilité en vélos adaptés et diffuser l’information sur cette aide à l’achat.

## Notre apport :

+ Expertise sur la thématique vélo & handicap
+ Connaissances techniques de ces vélos spéciaux
+ Connaissances de l’écosystème mobilité et handicap
+ Capacité à synthétiser la diversité des vélos adaptés et des aides financières et les rendre intelligibles pour le client et le public cible
+ Conception d’outils de communication (affiche et flyer)
+ Conception et animation d’ateliers de sensibilisation
