---
title: Un vélo pour tous
map: true
details: 
  Commanditaire: 
    nom: Montpellier Méditerranée Métropole
    nature: Maître d’ouvrage
  Equipe: "[SuperVitus305](/historique)"
  Budget: 25000
  Date: 2019   
---

La Métropole montpelliéraine a confié à [SuperVitus305](/historique) l’organisation et l’animation d’un événement sur 2 jours afin de questionner et sensibiliser les citoyens, les acteurs associatifs et professionnels sur la thématique de la mobilité active de tous.
{.intro}

Cette action s’est inscrite dans la stratégie et la politique vélo de la Métropole. Elle a permis une prise de contact avec la population et les acteurs locaux au sujet de la mobilité active, notamment auprès des publics éloignés de ces modes de déplacement. En effet, une politique cyclable ambitieuse s’appuie sur un processus de consultation ouvert et inclusif. En rassemblant les principales parties prenantes, associations, experts et usagers, cet événement a permis de recueillir les besoins des publics éloignés de la mobilité active, des usagers et futurs usagers et de fédérer la population et les acteurs locaux sur le sujet de la mobilité en vélo pour tous. Ces 2 jours auront aussi permis de se faire rencontrer acteurs de la mobilité avec ceux du médico-social, première brique d'une politique vélo plus inclusive.

## Étapes du projet

+ Création des partenariats avec les acteurs locaux
+ Conception des outils pour les ateliers participatifs
+ Animation des ateliers participatifs
+ Mise en place d’une animation grand public avec flotte de vélos adaptés aux différents besoins, parcours d’essai et parcours de vélo-école, animations pour petits et grands