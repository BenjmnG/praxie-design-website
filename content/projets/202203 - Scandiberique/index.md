---
title: Labellisation Tourisme & Handicap d’une voie verte
auto_initié: false
map: true
details: 
  Commanditaire: 
    nom: Département du Loiret 
    nature: Maître d’ouvrage
  Equipe: ["Praxie Design", "Inddigo", "Ingérop"]
  Budget: 
  Date: 2021
---

Diagnostic et préconisations d’aménagements nécessaires pour la labellisation Tourisme & Handicap d’une portion de la Scandibérique.
{.intro }

Le Département du Loiret souhaite labelliser Tourisme & Handicap (4 types de handicap) une portion de la Scandibérique. La mission a été confiée à un groupement d'entreprises : Inddigo, Ingérop et Praxie Design. Notre mission a été de diagnostiquer l’existant pour les 4 typologies de handicap, et d’apporter des préconisations en termes d’aménagements. La question de l’usage pour tous les types d'usagers, y compris les familles et les personnes âgées, et le sujet de l’attractivité de l’itinéraire ont aussi été abordés et questionnés avec le Département.