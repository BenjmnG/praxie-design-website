---
title: Etude des mobilités durables et inclusives en territoire peu dense
auto_initié: false
map: true
details: 
  Commanditaire: 
    nom: Val d'Ille-Aubigné (Communauté de communes)
    nature: Maître d’ouvrage
  Equipe: ["Praxie Design", "COPOMO"]
  Budget: 
  Date: 2021
---


Dans l'objectif de développer les mobilités durables et de les rendre accessibles à toutes et tous quels que soient ses moyens et ses capacités, la communauté de communes Val d'Ille-Aubigné, au nord de Rennes, nous a sollicités, en collaboration avec le bureau d'études COPOMO.
{.intro }

La première phase de cette étude a consisté en un diagnostic de l'existant sur la base d'une analyse de documents et d'une immersion de 2 jours sur le territoire, en équipe avec COPOMO. Sur place, nous avons parcouru le territoire à vélo, rencontré les acteurs locaux et les habitants. Nous avons pu observer des initiatives intéressantes déjà en place pour favoriser les mobilités actives et partagées. 

Suite à cela, nous avons pu co-construire un plan d’actions avec 13 fiches-action simples et concrètes pour encourager ces mobilités, réparties en 3 axes :

1. Fédérer et former les élus et les citoyens
2. Développer des services inclusifs couvrant le territoire
3. (Re)dessiner des aménagements inclusifs sur le territoire