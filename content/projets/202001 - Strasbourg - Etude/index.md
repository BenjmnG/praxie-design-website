---
title: Conception de plans piétons
map: true
details: 
  Commanditaire: 
    nom: Eurométropole de Strasbourg
    nature: Maître d’ouvrage
  Equipe: ['Inddigo', '[SuperVitus305](/historique)', 'Artgineering']
  Budget: Accord cadre
  Date: 2020 — 2021
---

En collaboration avec Inddigo, conception de plans piétons pour les 600 abris bus/tram du réseau de transports de l’Eurométropole de Strasbourg
{.intro}

L’Eurométropole de Strasbourg souhaite équiper 600 abris bus/tram de son réseau de transports en commun de plans de quartiers adaptés aux modes actifs et d’affiches de communication pour inciter les usagers à adopter l’attitude véloptimiste. Nous les avons accompagné sur la définition des besoins, la stratégie et le design des cartes en étroite collaboration avec Inddigo.

Ce projet s'inscrit dans le cadre d'une collaboration entre [SuperVitus305](/historique), Inddigo et Artgineering pour donner un nouvel élan au développement des modes actifs sur l’ensemble du territoire de l’Eurométropole de Strasbourg.

Grace à des politiques volontaristes, les parts modales de la marche et du vélo sont très élevées au sein de l’Eurométropole. Pourtant, et malgré les efforts déployés, les pratiques stagnent ou, tout au moins, n’augmentent pas au rythme souhaité à l’échelle de l’agglomération.

Il s’agit là de l’enjeu stratégique majeur de la mission confiée à [SuperVitus305](/historique), Inddigo et Artgineering : réussir à déployer sur l’ensemble de l’agglomération, et pour l’ensemble de la population, la dynamique observée dans le centre et sur certaines catégories de la population.

