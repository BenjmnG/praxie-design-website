---
title: AccesCycle
map: false
details: 
  Equipe: Camille Pechoux pour le CEREMH
  Date: 2011 — 2017
---

Le projet AccesCycle propose différents services autour de la thématique vélo & handicap :

+ Base de données sur les cycles adaptés en libre accès
+ Informations sur les vélos selon le type d’utilisation (seul, accompagné, avec enfants...)
+ Rubrique d’informations pratiques, dont une sur les aides au financement, mais aussi la location de vélos adaptés, les occasions...
+ Conseil aux personnes à mobilité réduite et aux professionnels de la réadaptation, par téléphone et mail
+ Atelier Mobilités auprès des séniors, avec une compagnie de théâtre (soutenus par les conférences des financeurs)
+ Organisation d’essais de vélos adaptés, avec l’équipe pluridisciplinaire le cas échéant (avec le soutien de la Fondation de France)
+ Organisation d’animations autour de la mobilité sur vélo adapté lors d’événements (type fête du + lo, journée du handicap...) : présentation de vélos, essais, conseils auprès du grand public, table ronde avec les acteurs de la mobilité...
+ Offre de formations : pour les personnes à mobilité réduite, les professionnels de la réadaptation, les revendeurs de cycles, les services techniques. 
+ Conseil aux collectivités (aménagements cyclables, mise en place de services...)
+ Conseil aux industriels (innovation, aide à la conception)
+ Conseil aux opérateurs de transport (conception, accessibilité, intermodalité)

Projet soutenu par la Fondation de France 
Lauréat Talent du Vélo Pédagogie 2013 du Club de Villes et Territoires Cyclables