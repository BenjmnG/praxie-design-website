---
title: Expérimentation Gares & Mobilité active
auto_initié: false
map: true
details: 
  Commanditaire: 
    nom: Gare & Connexions
    nature: Maître d’ouvrage
  Equipe: ["Praxie Design", "Kisio", "Béô Design"]
  Budget: 
  Date: 2022
legendes: 
  1: Borne d'accueil des usagers sur le parvis de la Gares
  2: Signalétique
  3: Rampe d'accès vélo

---


Fin juin 2022, s’est déroulée en gare de Pontcharra (Isère) la phase test grandeur nature du projet “Gares & Mobilité active”, auquel nous avons participé pour la conception des services, le design des dispositifs, la fabrication des prototypes et l’animation.
{.intro }

Pendant 2 jours, les parties prenantes (SNCF, SNCF Gares & Connexions, collectivités locales et acteurs locaux de la mobilité) ainsi que les usagers ont pu découvrir et donner leur avis sur des propositions concrètes pour développer les modes actifs et partagés en lien avec le train, et faciliter les trajets dans la vallée du Grésivaudan.

Les enjeux design étaient triples :
- Inciter aux changements de comportement en mettant en valeur les alternatives à l’auto-solisme.
- Apaiser l’espace public et améliorer le confort et la sécurité des usagers les plus fragiles
- Permettre aux usagers de mieux visualiser et comprendre les services en gare

Les solutions que nous avons imaginées pour répondre à ces enjeux ont été co-construites à partir d'entretiens, d’ateliers et d’un questionnaire. Les usagers ont aussi été sollicités au moment du démonstrateur.


Toutes ces solutions ont été pensées pour être accessibles à tous et notamment aux plus fragiles, conformément aux valeurs que nous portons et aux besoins sociétaux. Elles devront être testées de manière plus poussée pour être validées au regard de l'accessibilité et de leur efficacité, avant d'être potentiellement déployées. Elles offrent déjà des perspectives intéressantes et innovantes pour répondre aux enjeux posés, pour ré-enchanter l'espace public et redonner confiance à l'usager quant à sa place en son sein.

L’ensemble des prototypes ont été conçus et fabriqués dans nos locaux à Villeurbanne, dans une démarche éco-responsable, avec des matériaux non toxiques (peinture), bio-sourcés (bois) et recyclables (papier).

- Commanditaire : SNCF Gares & Connexions
- Pilotage : Kisio
- Co-conception des services : Kisio et Praxie Design
- Animation : Kisio et Praxie Design
- Design des dispositifs : Praxie Design et Béô Design
- Fabrication des prototypes : Béô Design
- Crédit photos : Praxie Design