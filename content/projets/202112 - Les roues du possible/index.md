---
title: Série de documentaires "Les Roues du Possible"
auto_initié: true
map: true
details: 
  Commanditaire: 
    nom: Malakoff Humanis
    nature: Mécène
  Equipe: Praxie Design
  Budget: 
  Date: 2022
---

Lors de la création de Praxie Design, nous avons décidé qu'une partie de notre temps serait dédié à la création de projets et de contenus participant à faire évoluer les pratiques de mobilité. 
Nous avons alors eu envie de tendre le micro aux personnes ayant des besoins particuliers qui pratiquent le vélo. L'idée d'un film nous est venue, afin de montrer et faire entendre leurs expériences. Logiquement nous nous sommes tournés vers Marc Aderghal, réalisateur et ami de longue date, et Paule Cornet pour la musique, tous deux amoureux de la petite reine.
{.intro }

**Premier épisode Les Roues du Possible, sorti en février 2022**

Ce film est plus qu’un plaidoyer pour le vélo comme mode de déplacement efficace, non polluant et bon pour la santé. Il va au-delà aussi d’un simple appel à l’inclusion des personnes en situation de handicap dans tous les pans de notre société. 
Ce film est surtout une invitation à regarder de côté, à voir le monde d’une autre manière, un monde où chacun a sa place quels que soient ses besoins, un monde dans lequel se déplacer n’est pas uniquement aller d’un point A vers un point B, mais une expérience plaisante, et conviviale, ce que le vélo rend possible. Par leurs témoignages, les protagonistes du film nous montrent comment ils sont passés, dans leur rapport au vélo, d’une impossibilité supposée à un autre possible, qu’il leur a fallu inventer.


La réalisation de ce film a été soutenu financièrement par l'Action Sociale Retraite de Malakoff Humanis Territoire Sud-Est. 
Le film est distribué sous licence libre de droit (Attribution NonCommercial NoDerivatives 4.0 International CC BY-NC-ND 4.0) et disponible sur le site [Les roues du possible](https://lesrouesdupossible.fr).

Un deuxième épisode est en cours de réalisation. Il donnera la parole à des personnes qui font le choix de vieillir à vélo !