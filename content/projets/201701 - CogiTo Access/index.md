---
title: CogiTo Access
map: false
details: 
  Commanditaire: PREDIT en collaboration avec le CEREMA, le CG 59 et le CG 78
  Equipe: Camille Pechoux pour le CEREMH
  Date: 2017
---

Participation au projet de recherche CogiTo Access - Cognition et réseau d’information : une condition du niveau de service du système de mobilité mutimodale pour tous.
{.intro}

Le projet de recherche CogiTo Access vise une meilleure compréhension des obstacles environnementaux à la mobilité des personnes ayant des incapacités intellectuelles. L’identification d’un ensemble cohérent de besoins a permis de créer un référentiel composé de recommandations sur le traitement de la chaine d’information destineé aux différents acteurs responsables des transports en commun et de l’urbanisme.