---
title: Formation des experts de l’ADMA
map: true
auto_initie: false
details: 
  Commanditaire: 
    nom: FUZO (ROZO + FUB)
    nature: Maître d’ouvrage
  Equipe: Praxie Design
  Budget: 30000
  Date: 2021
---

Nous avons formé les experts de l’Académie des Mobilités Actives (ADMA) sur le sujet de l’inclusion dans les déplacements à pied et à vélo, afin qu'ils puissent à leur tour sensibiliser les professionnels et le grand public lors de leurs formations et lors de leurs productions de contenu. 
{.intro}

Pendant 5 jours consécutifs, nous les avons immergés dans cette thématique au moyen de mises en situation (avec des vélos spéciaux et du matériel médical entre autres), d’échanges avec des usagers, de rencontres avec les acteurs locaux et de découverte d’aménagements cyclables.
Nous avons choisi d’aborder le thème de l’inclusion de manière très large en incluant aussi bien les personnes vieillissantes, les personnes en situation de handicap (moteur, sensoriel, cognitif...) que les familles avec des enfants et les jeunes de l’école de la 2e chance.
La question du design inclusif a aussi été au centre de nos échanges car l’inclusion n’a de sens que si elle se matérialise de manière concrète dans la conception des services, des aménagements et des produits qui composent notre environnement.
