---
title: SAMVA - à chacun son vélo 
auto_initié: true
map: true
details: 
  Commanditaire: 
    nom: Métropole de Lyon
    nature: Soutien financier
  Equipe: Praxie Design
  Budget: 30000
  Date: 2020 — 2021
---

Praxie Design a conçu et mis en place un centre expérimental de conseil et d'essais à la mobilité en vélos adaptés, dans l'objectif de faire du territoire de la Métropole de Lyon une zone pilote d’un système vélo conçu pour tous.
{.intro}



Le 1er volet de cette mission s’est focalisé sur la sensibilisation de l’écosystème et l’accompagnement des particuliers souhaitant changer de comportement de mobilité, pour trouver le vélo adapté à leurs besoins.
  
Pour cela, nous avons conçu un carnet d'informations sur les différents vélos permettant de répondre aux personnes ayant des besoins particuliers (téléchargeable gratuitement sur www.achacunsonvelo.fr).

Nous avons rassemblé une flotte d’une quinzaine de vélos (bicycles spéciaux, tricycles en tout genre, handbike, draisines pour adultes...) et mis en place un protocole d’essai. Nous avons réalisé une soixantaine d’accompagnements d'adultes à mobilité réduite et de personnes vieillissantes, depuis du conseil à distance jusqu’à des parcours complet (bilan mobilités, essais comparatifs, test à domicile, mise en lien avec la vélo-école, recherche de financements). 
En parallèle, nous avons conçu et animé des ateliers théoriques et pratiques, destinés aux professionnels médico-sociaux et de la mobilité (ergothérapeutes de la MDPH, vélo-école…) afin de les sensibiliser sur le sujet et ainsi de faciliter le parcours des usagers (financement du vélo, stage de vélo-école...).

Suite aux conclusions de cette expérimentation, un 2e volet est à l’étude : la mise en place d’un système de location de vélos adaptés pour les usagers avec des besoins particuliers, adossé à ce centre de conseil et d'essais.

## Livrables :

+ Un site internet dédiée : www.achacunsonvelo.fr
+ Un carnet d’information sur la diversité des vélos
+ Une évaluation à J+6mois et J+12mois