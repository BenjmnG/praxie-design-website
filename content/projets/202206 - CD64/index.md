---
title: "Création d'un service de prêt de vélos adaptés"
auto_initié: false
map: true
details: 
  Commanditaire: 
    nom: Département des Pyrénées-Atlantiques
    nature: Maître d’ouvrage
  Equipe: Praxie Design
  Budget: 
  Date: 2022
---

En octobre 2022, le Département des Pyrénées-Atlantiques inaugurait un service innovant, la mise à disposition d'une flotte qualitative de vélos adaptés aux personnes ayant des besoins particuliers, de bicycles spéciaux jusqu'au triporteur pour personne en fauteuil roulant, en passant par des tricycles et tandems. 
{.intro }

Nous avons accompagné l'équipe dynamique de la Mission vélo de ce département (Sabina Etcheverry et laurence Pauly notamment) en tant qu'assistants à maîtrise d'ouvrage sur la conception de ce service : diagnostic du territoire, de l'écosystème local et des besoins des usagers, recherche des partenaires locaux (mécanicien vélo, Maison Sport Santé, professionnels médico-sociaux...), préconisations pour la flotte de vélos et le service à mettre en place...

Pour la partie juridique, nous avons sollicité un de nos partenaires de longue date, le bureau d'études Inddigo.

Nous avons également accompagné le Département sur la stratégie de marque “vélo pour tous” avec pour objectif de définir la pertinence d’une telle marque, les besoins, et surtout ses contours : définition, critères et applications.

Enfin, nous avons produit une série d'outils de diagnostic et d'information sur l'accessibilité des voies vertes en lien avec le service de mise à disposition de vélos adaptés.