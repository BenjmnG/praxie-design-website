---
title: Étude d’implantation des services vélo sur la V50
map: true
details: 
  Commanditaire: 
    nom: Comité d’itinéraire de la V50 - La Voie Bleue
    nature: Maître d’ouvrage
  Équipe: Praxie Design et Fractale
  Montant de la mission: 1000
  Date: 2020
---

Praxie Design, en partenariat avec Fractale, a mis en place une méthode innovante de diagnostic des services et rédigé un schéma directeur de la Voie Bleue entre Luxembourg et Lyon.
{.intro} 

La première partie de cette mission a été de faire une synthèse des données disponibles en matière d’équipement pour les usagers du vélo afin de proposer une évaluation technique et qualitative de l’offre. Fractale et Praxie Design, en collaboration avec Vélo & Territoires, ont déployé un outil permettant aux gestionnaires de la V50 de collecter des données géolocalisées sur leur territoire. 

Cette synthèse a été complétée par une reconnaissance terrain en vélo pendant 10 jours, qui a aussi permis de nous imprégner du territoire et de vivre l’expérience usager. Ces deux approches ont contribué à définir un schéma directeur des équipements nécessaires pour compléter l’offre de services faite aux utilisateurs de la voie verte. Les questions d’accessibilité des services aux personnes à mobilité réduite et aux personnes vieillissantes, ainsi que l’intégration dans le paysage, ont été déterminantes dans nos préconisations.