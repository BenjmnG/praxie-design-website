---
title: Chronovélo
map: true
details: 
  Commanditaire: 
    nom: Grenoble Alpes Métropole
    nature: Maître d’ouvrage
  Equipe: "[SuperVitus305](/historique)"
  Montant de la mission: 54754
Date: 2016 — 2017 
---

[SuperVitus305](/historique) a été missionné par Grenoble-Alpes Métropole sur un projet ambitieux : déployer sur le territoire de la métropole grenobloise un réseau cyclable à haut niveau de service.
{.intro}

Chronovélo est le réseau cyclable structurant de la Métropole avec un système performant et sécurisant de pistes bidirectionnelles et hors du flux routier. Lancé en juin 2017, il est composé de quatre axes totalisant 40 km de liaisons cyclables entre les communes de la Métropole. L’identité visuelle, le système signalétique et les aires de services conçus par [SuperVitus305](/historique) ont un objectif : sécuriser et faciliter les déplacements à vélo pour une diversité d’usagers. Notre travail a permis de mettre en place un espace dédié aux usagers à vélo sur la voie publique, avec des codes qui leur sont propres.

Cet itinéraire a pour ambition de devenir un réseau de transport à part entière, de donner au vélo une place nouvelle dans l’espace public et d’être un projet pionnier en matière de développement du vélo en ville.