---
title: "Extension du service de location Véligo aux vélos adaptés"
auto_initié: false
map: true
details: 
  Commanditaire: 
    nom: Ile-de-France Mobilités
    nature: Maître d’ouvrage
  Equipe: Praxie Design
  Budget: 
  Date: 2022-2024
---

Depuis 2020, nous accompagnons Ile-de-France Mobilités dans l’ouverture de sa politique vélo aux personnes ne pouvant pas faire de vélo classique pour raisons de santé.
{.intro }

En 2022, nous avons réalisé une étude d’extension des services vélos, en coopération avec le bureau d’études Inddigo, dans l'objectif - entre autres - d’élargir la location de vélos Véligo à d’autres publics cibles. Nous avons traité la question de la location de vélos adaptés aux personnes ne pouvant faire de vélo classique pour raisons de santé.

Ce que notre équipe a fait :
- benchmark,
- entretiens avec des usagers et des professionnels,
- appui pour l’organisation,
- recrutement et animation de focus group (en collaboration avec l’IFOP),
- création des parcours usagers,
- choix des vélos et des publics cibles,
- définition du service d’accompagnement par des professionnels formés,
- définition des services attendus dans les maisons du vélo et de l’accessibilité de ces dernières,
- stratégie de communication ;

Suite à cette étude, en 2023 nous sommes assistants à maîtrise d’ouvrage pour Ile-de-France Mobilités dans le cadre du renouvellement du contrat Véligo Location, sur la partie des vélos adaptés aux personnes ne pouvant pas faire de vélo classique pour raisons de santé : rédaction du contrat de renouvellement de la DSP Véligo Location, définition des critères de notation, réponse aux questions des candidats, analyse des offres…

