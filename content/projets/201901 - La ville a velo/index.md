---
title: Identité visuelle pour une association d'usagers vélo
map: true
details: 
  Commanditaire: La Ville à Vélo
  Equipe: Thomas Rosset
  Budget: 4000
  Date: 2019
---

Identité visuelle et outils de communication pour l’association La Ville à Vélo de la Métropole de Lyon
{.intro}

La première partie de ce travail a consisté à concevoir et animer un atelier collaboratif afin de faire émerger un consensus sur le positionnement et la stratégie de l’association pour les 5 à 10 ans à venir.

Sur la base de l’orientation stratégique définie ensemble, nous avons ensuite conçu une identité visuelle comprenant un logo principal et un système de signature pour les antennes locales, une charte graphique et des outils de communication.